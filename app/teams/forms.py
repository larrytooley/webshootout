from flask_wtf import FlaskForm
from wtforms import (StringField, SubmitField)
from wtforms.fields import TextAreaField
from wtforms.validators import (InputRequired, EqualTo)
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from sqlalchemy.orm.exc import NoResultFound

from app.models import User, Team


class RegisterTeamForm(FlaskForm):
    name = StringField('Name', validators=[
        InputRequired(message="Please enter a name.")])
    description = TextAreaField('Description', validators=[
        InputRequired(message="Please enter a description.")])
    members = QuerySelectField(query_factory=User.get, allow_blank=True)
    leader = QuerySelectField(query_factory=User.get, allow_blank=True)
    submit = SubmitField('Submit')

    def validate(self):
        if not super().validate():
            return False
        else:
            try:
                team = Team.get(name=self.name.data.title().strip())
            except NoResultFound:
                return True
            else:
                return False
