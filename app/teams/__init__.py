from flask import Blueprint


teams = Blueprint('teams', __name__, template_folder='../templates/teams')

from .import controller, errors  # nopep8
