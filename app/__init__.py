from flask import Flask, render_template
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_migrate import Migrate
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_wtf.csrf import CSRFProtect

from config import config
from .utils import format_datetime


db = SQLAlchemy()
lm = LoginManager()
bcrypt = Bcrypt()
migrate = Migrate()
admin = Admin(name='admin', template_mode='bootstrap3')
csrf = CSRFProtect()


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    lm.init_app(app)
    lm.login_view = "user.login"
    bcrypt.init_app(app)
    migrate.init_app(app, db)
    csrf.init_app(app)

    admin.init_app(app)

    from .models import User, Team, Category, Event
    admin.add_view(ModelView(User, db.session, endpoint='users'))
    admin.add_view(ModelView(Team, db.session, endpoint='team'))
    admin.add_view(ModelView(Event, db.session, endpoint='event'))
    admin.add_view(ModelView(Category, db.session, endpoint='category'))

    app.jinja_env.filters['format_datetime'] = format_datetime

    db.init_app(app)

    from .main import main as main_module
    from .user import user as user_module
    from .events import events as events_module
    from .teams import teams as teams_module

    app.register_blueprint(main_module)
    app.register_blueprint(user_module)
    app.register_blueprint(events_module)
    app.register_blueprint(teams_module)

    return app
