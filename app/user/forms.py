from flask_wtf import FlaskForm
from wtforms import (StringField, PasswordField, SubmitField)
from wtforms.validators import (InputRequired, Email, EqualTo)
from sqlalchemy.orm.exc import NoResultFound

from app.models import User


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[
        InputRequired(message="Please enter an email address."), Email()])
    password = PasswordField('Password', validators=[
        InputRequired(message="Please enter a password.")])
    submit = SubmitField("Login")

    def validate(self):
        if not super().validate():
            return False
        else:
            user = User.get(email=self.email.data.lower().strip())

            if user and user.check_password(self.password.data):
                return True
            else:
                self.password.errors.append("Invalid password.")
            return False


class RegisterUserForm(FlaskForm):
    firstname = StringField('First Name', validators=[
        InputRequired(message="Please enter your firstname.")])
    lastname = StringField('Last Name', validators=[
        InputRequired(message="Please your lastname.")])
    email = StringField('Email', validators=[
        InputRequired(message="Please enter an email address."), Email()])
    password = PasswordField('Password', validators=[
        InputRequired(message="Please enter a password."),
        EqualTo('confirm', message="Passwords must match.")])
    confirm = PasswordField('Confirm Pasword', validators=[
        InputRequired(message="Please enter a password again.")])
    submit = SubmitField('Register')

    def validate(self):
        """
        """
        if not super().validate():
            return False
        else:
            try:
                user = User.get(email=self.email.data.lower().strip())
            except NoResultFound:
                return True
            else:
                self.email.errors.append("That email already exists.")
                return False


class ForgotPasswordForm(FlaskForm):
    email = StringField('Email', validators=[
        InputRequired(message="Please enter an email address."), Email()])
    submit = SubmitField('Send Reset Email')


class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[
        InputRequired(message="Please enter a password."),
        EqualTo('confirm', message="Passwords must match.")])
    confirm = PasswordField('Confirm Pasword', validators=[
        InputRequired(message="Please enter a password again.")])
    submit = SubmitField('Reset Password')
