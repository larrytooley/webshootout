import os
import datetime
import logging

from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.ext.declarative import declared_attr, declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy import create_engine
from sqlalchemy.sql.expression import func

from itsdangerous import (BadSignature, SignatureExpired)

from app import db, bcrypt
from app.utils import (serializer, timed_serializer)


class DBMixin:
    """
    Mixin for basic DB methods and atributes.
    """

    @declared_attr
    def __tablename__(cls):
        return "{}s".format(cls.__name__.lower())

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    date_created = db.Column(db.DateTime, nullable=False,
                             default=datetime.datetime.utcnow)
    date_modifiied = db.Column(db.DateTime, nullable=False,
                               default=datetime.datetime.utcnow,
                               onupdate=datetime.datetime.utcnow)

    @classmethod
    def get(cls, id=None, **kwargs):
        """
        If `id` passed, it queries for the id and returns exactly 1 result.
        If anything else is passed, returns a list of items matching query.
        """
        if not id and not kwargs:
            obj = db.session.query(cls).all()
        if id:
            obj = db.session.query(cls).filter_by(id=id).first()
        if kwargs:
            obj = [item for item in
                   db.session.query(cls).filter_by(**kwargs).all()]
        if not obj:
            logging.info('NoRestultFound')
            return NoResultFound
        return obj

    @classmethod
    def get_or_404(cls, id=None):
        obj = db.session.query(cls).filter_by(id=id).first_or_404()
        return obj

    @classmethod
    def create(cls, obj):
        """
        Adds object to sessoin and returns object from DB.

        Was used to simplifiy adding to DB and make it harder to forget to
        close connection. It also returned the object after it was added to
        the DB with it's `id`. However this may or may not work correctly now.
        """
        db.session.add(obj)
        # db.session.flush()
        db.session.commit()
        obj = cls.get(id=obj.id)
        logging.info('Got or created item: {}'.format(obj))
        return obj

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.get(id=self.id)

    @classmethod
    def _unique(cls, session, arg, kwargs):
        """
        Creates or gets unique object from DB.

        Checks uniqueness based upon `cls.unique_hash` and `cls.unique_filter`.
        If unique it adds it to DB. If not unique it returns the previous item
        already in DB.
        """
        cache = getattr(session, '_unique_cache', None)

        if cache is None:
            session._unique_cache = cache = {}

        key = (cls, cls.unique_hash(*arg, **kwargs))
        if key in cache:
            return cache[key]
        else:
            with session.no_autoflush:
                q = session.query(cls)
                q = cls.unique_filter(q, *arg, **kwargs)
                obj = q.first()
                if not obj:
                    obj = cls(*arg, **kwargs)
                    session.add(obj)
            cache[key] = obj
            return obj

    @classmethod
    def unique_hash(cls, *arg, **kwargs):
        """
        Defines what makes an item unique.
        """
        raise NotImplementedError()

    @classmethod
    def unique_filter(cls, query, *arg, **kwargs):
        """
        Creates query to test uniqueness of object.
        """
        raise NotImplementedError()

    @classmethod
    def as_unique(cls, *arg, **kwargs):
        """
        Gets or creats a unique object in the DB.
        """
        session = db.session
        return cls._unique(session, arg, kwargs)

    @classmethod
    def get_max(cls, query_by=None):
        max = db.session.query(func.max(query_by)).scalar()
        return max

    def __repr__(self):
        cls = self.__class__.__name__
        return '<{} id={}>'.format(cls, self.id)

    def __str__(self):
        cls = self.__class__.__name__
        return '<{} id={}>'.format(cls, self.id)


team_leader = db.Table('team_leader',
                       db.Column('team_id', db.Integer,
                                 db.ForeignKey('teams.id')),
                       db.Column('leader_id', db.Integer,
                                 db.ForeignKey('users.id')))

team_user = db.Table('team_user',
                     db.Column('team_id', db.Integer,
                               db.ForeignKey('teams.id')),
                     db.Column('user_id', db.Integer,
                               db.ForeignKey('users.id')))

event_team = db.Table('event_team',
                      db.Column('team_id', db.Integer,
                                db.ForeignKey('teams.id')),
                      db.Column('event_id', db.Integer,
                                db.ForeignKey('events.id')))

event_category = db.Table('event_category',
                          db.Column('event_id', db.Integer,
                                    db.ForeignKey('events.id')),
                          db.Column('category_id', db.Integer,
                                    db.ForeignKey('categorys.id')))


class User(DBMixin, db.Model):
    firstname = db.Column(db.String(120))
    lastname = db.Column(db.String(120))
    email = db.Column(db.String(120))
    _password = db.Column('password', db.String(120))
    confirmed = db.Column(db.Boolean())
    _last_seen = db.Column('last_seen', db.DateTime())
    _is_active = db.Column('is_active', db.String(50))

    def __init__(self, firstname, lastname, email, password):
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.password = password

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = bcrypt.generate_password_hash(password)

    def check_password(self, password):
        return bcrypt.check_password_hash(self.password, password)

    def is_authenticated(self):
        return True

    def is_annonymous(self):
        return False

    def activate(self):
        self.confirmed = datetime.datetime.utcnow()
        self._is_active = True

    def is_active(self):
        if not self.confirmed or not self._is_active:
            return False
        return True

    def get_id(self):
        # return self.email
        return self.id

    @property
    def last_seen(self):
        return self._last_seen

    @last_seen.setter
    def last_seen(self, date):
        self._last_seen = date
        return self.last_seen

    @classmethod
    def get(cls, id=None, email=None, is_active=True, **kwargs):
        if not id and not email and not kwargs:
            return [item for item in
                    db.session.query(cls).filter_by(is_active=is_active).all()]
        if id:
            obj = db.session.query(cls).filter_by(id=id).first()
        elif email:
            obj = db.session.query(cls).filter_by(email=email).first()
        elif kwargs:
            obj = [item for item in
                   db.session.query(cls).filter_by(
                       is_active=is_active, **kwargs).all()]
        else:
            obj = [item for item in
                   db.session.query(cls).filter_by(is_active=is_active).all()]
        if not obj:
            logging.info('NoResultFound')
            raise NoResultFound
        return obj

    @staticmethod
    def get_activation_link(user):
        user_id = user.get_id()
        serial = serializer()
        payload = serial.dumps(user_id)
        return payload

    @staticmethod
    def check_activation_link(payload):
        serial = serializer()
        try:
            user_id = serial.loads(payload)
        except BadSignature:
            return False
        return user_id

    @staticmethod
    def get_password_reset_link(user):
        user_id = user.get_id()
        serial = timed_serializer()

        # disallows password reset link to be reused
        old_hash = user.password[:10]
        payload = serial.dumps(user_id + old_hash)
        return payload

    @staticmethod
    def check_password_reset_link(payload):
        serial = timed_serializer()

        try:
            # disallows password reset link to be reused
            unhashed_payload = serial.loads(payload, max_age=86400)
            old_hash = unhashed_payload[
                len(unhashed_payload)-10:len(unhashed_payload)]
            user_id = unhashed_payload[:-10]
            user = User.get(email=user_id)
        except (SignatureExpired, BadSignature):
            return False
        return (user, old_hash)

    def __str__(self):
        return '{} {} - {}'.format(self.firstname, self.lastname, self.email)


class Team(DBMixin, db.Model):
    name = db.Column(db.String(120), nullable=False)
    description = db.Column(db.Text(), nullable=False)
    members = db.relationship('User',
                              secondary='team_user',
                              backref=db.backref('teams', lazy='dynamic'))
    leader = db.relationship('User',
                             secondary="team_leader",
                             backref="team_user")

    def __str__(self):
        return '{}'.format(self.name)


class Event(DBMixin, db.Model):
    title = db.Column(db.String(120), nullable=False)
    start_date = db.Column(db.DateTime(), nullable=False)
    end_date = db.Column(db.DateTime(), nullable=False)
    teams = db.relationship('Team',
                            secondary='event_team',
                            backref=db.backref('events', lazy='dynamic'))
    categories = db.relationship('Category',
                                 secondary='event_category',
                                 backref=db.backref('events', lazy='dynamic'))

    def __str__(self):
        return '{}'.format(self.title)


class Category(DBMixin, db.Model):
    name = db.Column(db.String(120), nullable=False)
    _weight = db.Column('weight', db.Integer(), nullable=False)

    @property
    def weight(self):
        return self._weight / 1000

    @weight.setter
    def weight(self, value):
        self._weight = value * 1000

    def __str__(self):
        return '{}'.format(self.name)
