import os
import logging

import secretstuff


BASE_DIR = os.path.abspath(os.path.dirname(__file__))


class Config:
    APP_NAME = "WebShootout"
    CSRF_ENABLED = True
    SECRET_KEY = secretstuff.SECRET_KEY
    TMP_DIR = os.path.join(BASE_DIR, 'tmp')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    logging.basicConfig(level=logging.INFO,
                        format='[%(levelname)s]: %(message)s')
    logging.getLogger('requests').setLevel(logging.WARNING)

    SQLALCHEMY_DATABASE_URI = (
        'sqlite:///' + os.path.join(Config.TMP_DIR, 'dev.sqlite'))


class TestingConfig(Config):
    TESTING = True

    SQLALCHEMY_DATABASE_URI = (
        'sqlite:///' + os.path.join(Config.TMP_DIR, 'test.sqlite'))


class ProductionConfig(Config):
    DEBUG = False
    TESTING = False

    SQLALCHEMY_DATABASE_URI = (
        'sqlite:///' + os.path.join(Config.TMP_DIR, 'prod.sqlite'))

config = {
    'development': DevelopmentConfig,
    'test': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig,
    }
